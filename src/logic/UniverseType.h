#ifndef UNIVERSE_TYPE_H
#define UNIVERSE_TYPE_H

enum class UniverseType {
  Hashlife,
  Life,
};

#endif
