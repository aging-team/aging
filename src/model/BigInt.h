#ifndef BIG_INT_H_
#define BIG_INT_H_

#include <gmpxx.h>

typedef mpz_class BigInt;

#endif // BIG_INT_H_
